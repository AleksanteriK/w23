import logo from './logo.svg';
import './App.css';
import Reporter from './Reporter';

function App() {
  return (
    <div className="App">
      <Reporter name = 'Antero Mertaranta'>Löikö mörkö sisään</Reporter>
      <Reporter name = 'Abraham Lincoln'>Whatever you are be a good one</Reporter>
    </div>
  );
}

export default App;
