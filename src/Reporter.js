export default function Reporter (props) {
   return (
      <div>
      {props.name}: {props.children}
      </div>
   )
}